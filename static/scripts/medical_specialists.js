function redirect_to_login() {
    location.href = "/logins";
}

function redirect_to_specialist(spec_id) {

    $.ajax({
        url : "get_specialist_with_spec/" + spec_id,
        dataType: "html",
        type : "GET", // http method
        // handle a successful response
        success : function(response) {
            let response_list = JSON.parse(response);

            if (response_list.length===0){
                alert("هیچ نتیجه ای یافت نشد!")
            }
            else if (response_list.length === 1) {
                let specialist_id = response[0];
                location.href = "doctor/" + specialist_id;
            }
            else {
                let path = "/specialist/";
                for (let i = 0; i < response_list.length; i++) {
                    if (i > 0) {
                        path = path + "," + response_list[i]
                    }
                    else {
                        path = path + response_list[i]
                    }

                }
                location.href = path;
            }
        },

        // handle a non-successful response
        error : function() {
            alert("خطا")
        }
    });
}
