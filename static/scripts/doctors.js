comment_list = [];
comment_current = -1;

function redirect_to_login() {
    location.href = "/logins";
}

function set_data(specialist_id) {


     $.ajax({
            url : "../get_specialist/" + specialist_id,
            type : "GET", // http method
            success : function(response) {

                let name = response.name;
                let spec_id = response.spec;
                let spec = "";

                let number = response.number;
                let address = response.address;
                let online_pay = response.online_pay;
                let experience_years = response.experience_years;
                let week_days = response.week_days;
                let phone = response.phone;

                bredcrum_spec = document.getElementById("way-2");
                bredcrum_spec.innerText = spec + " >";

                bredcrum_name = document.getElementById("way-3");
                bredcrum_name.innerText = name;

                right_parent = document.getElementById("middle-Part-drInfo-top-middleColumn");

                let avatar_img = document.createElement("img");
                avatar_img.src = "../static/images/doctors/dr.png";

                let name_h1 = document.createElement("h1");
                name_h1.innerText = name;

                let spec_p = document.createElement("p");
                spec_p.setAttribute("id", "p-spec");
                spec_p.innerText = spec;


                $.ajax({
                    url: "../get_speciality/" + spec_id,
                    type: "GET", // http method
                    success: function (response) {
                        document.getElementById("p-spec").innerText = spec = response.name;
                    }
                });


                let number_p = document.createElement("p");
                number_p.innerText = "شماره پرونده : " + number;

                right_parent.appendChild(avatar_img);
                right_parent.appendChild(name_h1);
                right_parent.appendChild(spec_p);
                right_parent.appendChild(number_p);

                let experience_p = document.getElementById("experience");
                experience_p.innerText = experience_years + " سال";

                let empty_date_p = document.getElementById("first-empty-date");
                empty_date_p.innerText = "25 بهمن";

                let online_p;
                if (online_pay==="True")
                {
                    online_p = "دارد";
                }
                else {
                    online_p = "ندارد";
                }

                let online_pay_p = document.getElementById("online-pay");
                online_pay_p.innerText = online_p;

                let number_comment = 0;
                let score = -1;
                $.ajax({
                    url : "../get_specialist_score", // the endpoint
                    type : "GET", // http method
                    data : {
                        specialist_id: specialist_id,
                    }, // data sent with the post request

                    // handle a successful response
                    success : function(response) {
                        if (response==="No comment"){
                            score = 0
                        }
                        else if (response==="Failed"){
                            alert("ERROR")
                        }
                        else{
                            score = parseFloat(response);

                        }
                        let rates_p = document.getElementById("rate");
                        console.log(score.toString());
                        rates_p.innerText = score.toString();

                        let stars = Number(score.toFixed(0));
                        let star_svg = document.getElementsByClassName("feather-star");
                        for (let i=0; i < stars; i++){
                            star_svg[i].style.fill = 'blue';
                        }
                    },
                    // handle a non-successful response
                    error : function() {
                        alert("ERROR")
                    }
                });


                let comments_number = document.getElementById("comments-number");
                comments_number.innerText = "از " + response.number_comment + "نظر ";

                comment_list = response.comment;


                if (response.number_comment ===0){
                    let commenter_p = document.getElementById("commenter");
                    commenter_p.innerText = "";

                    let comment_p = document.getElementById("comment");
                    comment_p.innerText = "نظری ثبت نشده است.\n اولین نفری باشید که نظر می دهد!";
                }
                else{
                    comment_current = -1;
                    display_comment()
                }

                let address_p = document.getElementById("address");
                address_p.innerText = address;

                let phone_p = document.getElementById("phone");
                phone_p.innerText = phone;


                // let week_days = [week_days_0, week_days_1, week_days_2,
                //     week_days_3, week_days_4, week_days_5 , week_days_6];

                let days = document.getElementsByClassName("day");
                for (let i=0; i<7; i++)
                {
                        if (week_days[i]==="False")
                        {
                             days[i].getElementsByClassName("tik")[0].classList.remove("appear");
                             days[i].getElementsByClassName("tik")[0].classList.add("hidden");

                             days[i].getElementsByClassName("untik")[0].classList.remove("hidden");
                             days[i].getElementsByClassName("untik")[0].classList.add("appear");

                        }
                        else {
                             days[i].getElementsByClassName("tik")[0].classList.remove("hidden");
                             days[i].getElementsByClassName("tik")[0].classList.add("appear");

                             days[i].getElementsByClassName("untik")[0].classList.remove("appear");
                             days[i].getElementsByClassName("untik")[0].classList.add("hidden");
                        }
                }

                let matab_title = document.getElementById("middle-Part-loaction-title1");
                let days_on_title = document.getElementById("middle-Part-loaction-title2");


                matab_title.onclick = function () {
                        matab_title.style.background = "white";
                        days_on_title.style.background = "#ededed";

                        let matab = document.getElementById("middle-Part-loaction-content");

                        matab.classList.remove("hidden");
                        matab.classList.add("appear");

                        let days_on = document.getElementById("middle-Part-loaction-content-days");

                        days_on.classList.remove("appear");
                        days_on.classList.add("hidden");

                };


                days_on_title.onclick = function () {
                        matab_title.style.background = "#ededed";
                        days_on_title.style.background = "white";

                        let matab = document.getElementById("middle-Part-loaction-content");

                        matab.classList.remove("appear");
                        matab.classList.add("hidden");

                        let days_on = document.getElementById("middle-Part-loaction-content-days");

                        days_on.classList.remove("hidden");
                        days_on.classList.add("appear");
                };

            },
            // handle a non-successful response
            error : function() {
                alert("ERROR")
            }
        });



}

function new_comment(specialist_id) {
    location.href = "../new_comment/" + specialist_id;
}

function display_comment(){
    comment_current ++;
    if (comment_current===comment_list.length){
        comment_current = 0;
    }

    let commenter_p = document.getElementById("commenter");
    commenter_p.innerText = "نظر " + comment_list[comment_current].owner;

    let comment_p = document.getElementById("comment");
    comment_p.innerText = comment_list[comment_current].text;

    setTimeout(display_comment, 5000);
}

function logout(){
    document.location = "./log_out"
}
