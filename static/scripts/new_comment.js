function submit(specialist_id) {

    $.ajax({
        url : "../new_comment_check", // the endpoint
        type : "POST", // http method
        data : {
            text:document.getElementById("text").value,
            score: document.getElementById("score").value,
            specialist_id: specialist_id,
        }, // data sent with the post request

        // handle a successful response
        success : function(response) {
            if (response==="Login required"){
                document.location = "/logins"
            }
            else if (response==="Successful"){
                document.location = "/doctor/" + specialist_id
            }
            else{
                alert("مشخصات را به درستی وارد کنید!")
            }
        },
        error : function() {
            alert("مشخصات را به درستی وارد کنید!")
        }
    });

}

function back(specialist_id){
    location.href = "/doctor/" + specialist_id;
}