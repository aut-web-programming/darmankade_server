function redirect_to_login() {
    location.href = "/logins";
}

function redirect_to_medicalSpecialists() {
    location.href = "/medical_specialists";
}

function redirect_to_specialist(spec_id) {

    $.ajax({
        url : "get_specialist_with_spec/" + spec_id,
        dataType: "html",
        type : "GET", // http method
        // handle a successful response
        success : function(response) {
            console.log(response);

            let response_list = JSON.parse(response);

            if (response_list.length===0){
                alert("هیچ نتیجه ای یافت نشد!")
            }
            else if (response_list.length === 1) {
                console.log(response[0]);
                console.log(response);
                let specialist_id = response[0];
                // location.href = "doctor/" + specialist_id;
            }
            else {
                let path = "/specialist/";
                for (let i = 0; i < response_list.length; i++) {
                    if (i > 0) {
                        path = path + "," + response_list[i]
                    }
                    else {
                        path = path + response_list[i]
                    }

                }
                location.href = path;
            }
        },

        // handle a non-successful response
        error : function() {
            alert("خطا")
        }
    });
}

function search() {
    let term = document.getElementById("search-select-input").value;

    $.ajax({
        url : "search_specialist", // the endpoint
        dataType: "html",
        type : "GET", // http method
        data : {
            term:term,
        }, // data sent with the post request

        // handle a successful response
        success : function(response) {
            let response_list = JSON.parse(response);
            console.log(response);

            if (response_list.length===0){
                alert("هیچ نتیجه ای یافت نشد!")
            }
            else if (response_list.length === 1) {
                location.href = "doctor/" + response_list[0];
            }
            else {
                let path = "/specialist/";
                for (let i = 0; i < response_list.length; i++) {
                    if (i > 0) {
                        path = path + "," + response_list[i]
                    }
                    else {
                        path = path + response_list[i]
                    }

                }
                location.href = path;
            }
        },

        // handle a non-successful response
        error : function() {
            alert("خطا")
        }
    });
}