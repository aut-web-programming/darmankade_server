
function submit_form() {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let address = document.getElementById("address").value;
    let experience_years = parseInt(document.getElementById("experience-years").value);
    let online_pay = document.getElementById("online-pay").checked;
    let phone = document.getElementById("phone").value;
    let spec = parseInt(document.getElementById("spec").value);
    let number = document.getElementById("number").value;

    let week_days_0 = document.getElementById("week_days_0").checked;
    let week_days_1 = document.getElementById("week_days_1").checked;
    let week_days_2 = document.getElementById("week_days_2").checked;
    let week_days_3 = document.getElementById("week_days_3").checked;
    let week_days_4 = document.getElementById("week_days_4").checked;
    let week_days_5 = document.getElementById("week_days_5").checked;
    let week_days_6 = document.getElementById("week_days_6").checked;

    $.ajax({
        url : "specialist_register_check", // the endpoint
        type : "POST", // http method
        data : {
            name:username,
            password: password,
            spec: spec,
            number: number,
            online_pay: online_pay,
            experience_years: experience_years,
            address: address,
            phone: phone,
            week_days:[
                week_days_0,
                week_days_1,
                week_days_2,
                week_days_3,
                week_days_4,
                week_days_5,
                week_days_6,
            ],
        }, // data sent with the post request

        // handle a successful response
        success : function(response) {
            if (response==="Successful"){
                document.location = "/"
            }
            else{
                alert("مشخصات را به درستی وارد کنید!")
            }

        },

        // handle a non-successful response
        error : function() {
            alert("مشخصات را به درستی وارد کنید!")
        }
    });

}
