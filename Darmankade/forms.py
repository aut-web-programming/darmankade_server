from django import forms
from .models import Patient, Specialist


class LoginForm(forms.Form):
    username = forms.CharField(label="username", max_length=64)
    password = forms.CharField(label="password", max_length=12)


class PatientRegisterForm(forms.Form):
    username = forms.CharField(label="username", max_length=64)
    phone = forms.CharField(label="phone", max_length=11)
    password = forms.CharField(label="password", max_length=12)


class CommentForm(forms.Form):
    text = forms.CharField(label="text", max_length=256)
    score = forms.IntegerField(label="score", min_value=1, max_value=5)
