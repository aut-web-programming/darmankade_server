from django.db import models
from django.contrib.auth.models import User, AbstractUser


class User(AbstractUser):
    is_patient = models.BooleanField(default=False)
    is_specialist = models.BooleanField(default=False)
    phone = models.CharField(max_length=11)


class Speciality(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return "تخصص " + self.name


class Patient(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return "بیمار " + self.user.username


class Specialist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    spec = models.ForeignKey(Speciality, on_delete=models.CASCADE)
    number = models.IntegerField()
    online_pay = models.BooleanField()
    experience_years = models.IntegerField()
    address = models.CharField(max_length=256)

    week_days_0 = models.BooleanField()
    week_days_1 = models.BooleanField()
    week_days_2 = models.BooleanField()
    week_days_3 = models.BooleanField()
    week_days_4 = models.BooleanField()
    week_days_5 = models.BooleanField()
    week_days_6 = models.BooleanField()

    def __str__(self):
        return "دکتر " + self.user.username


class Comment(models.Model):
    # FIXME: check and select good name for foreign key
    patient_id = models.ForeignKey(Patient, on_delete=models.CASCADE)
    specialist_id = models.ForeignKey(Specialist, on_delete=models.CASCADE)
    text = models.CharField(max_length=256)
    score = models.IntegerField()

    def __str__(self):
        return "نظر " + self.patient_id.user.username + " نسبت به " + self.specialist_id.user.username
