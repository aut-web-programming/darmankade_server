from django.db import IntegrityError

from django.shortcuts import render, get_object_or_404
from .models import Specialist, Patient, Comment, Speciality
from .forms import LoginForm, PatientRegisterForm, CommentForm
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout, get_user_model


def index(request):
    return render(request, 'index.html')


def logins(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            if request.user.is_patient:
                return HttpResponseRedirect("patient_profile")
            elif request.user.is_specialist:
                return HttpResponseRedirect("specialist_profile")
            else:
                return HttpResponseRedirect("select_role")
        else:
            return HttpResponseRedirect("select_role")
    else:
        return HttpResponse("Failed")


def specialist_login(request):
    return render(request, 'specialist_login.html')


def patient_login(request):
    return render(request, 'patient_login.html')


def patient_register(request):
    return render(request, 'patient_register.html')


def specialist_register(request):
    return render(request, 'specialist_register.html')


def register_check(request):
    if request.method == "POST":
        form = PatientRegisterForm(request.POST)

        if form.is_valid():
            try:
                user = get_user_model().objects.create_user(request.POST["username"], None, request.POST["password"])
                user.phone = request.POST["phone"]
                user.is_patient = True
                user.is_specialist = False
                user.save()

                patient = Patient(user=user)
                patient.save()
            except IntegrityError:
                return HttpResponseRedirect("register")

            return HttpResponseRedirect("/")
        else:
            return HttpResponseRedirect("register")
    else:
        return HttpResponseRedirect("register")


@csrf_exempt
def specialist_register_check(request):
    if request.method == "POST":
        try:
            speciality = get_object_or_404(Speciality, pk=request.POST["spec"])
            week_days = request.POST.getlist("week_days[]")

            user = get_user_model().objects.create_user(request.POST["name"], None, request.POST["password"])
            user.phone = request.POST["phone"]
            user.is_patient = False
            user.is_specialist = True
            user.save()

            specialist = Specialist(user=user, address=request.POST["phone"], number=request.POST["number"],
                                    online_pay=request.POST["online_pay"] == "true", spec=speciality,
                                    experience_years=int(request.POST["experience_years"]),
                                    week_days_0=week_days[0] == "true",
                                    week_days_1=week_days[1] == "true", week_days_2=week_days[2] == "true",
                                    week_days_3=week_days[3] == "true", week_days_4=week_days[4] == "true",
                                    week_days_5=week_days[5] == "true", week_days_6=week_days[6] == "true")
            specialist.save()
            print("specialist created successfully")

            return HttpResponse("Successful")

        except (IntegrityError, ValueError, AttributeError):
            return HttpResponse("specialist_register")
    else:
        return HttpResponse("specialist_register")


def doctor(request, specialist_id):
    specialist = get_object_or_404(Specialist, pk=specialist_id)
    speciality = specialist.spec.name
    return render(request, 'doctor.html', {'specialist': specialist, 'speciality': speciality})


def medical_specialists(request):
    return render(request, 'medical_specialists.html')


def specialists(request, specialist_id_list):
    if request.method == "GET":
        return render(request, 'specialist.html', {"specialist_id_list": specialist_id_list})


def patient_login_check(request):
    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid():

            user = authenticate(request, username=request.POST["username"], password=request.POST["password"])

            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/patient_login")
        else:
            return HttpResponseRedirect('/patient_login')
    else:
        return HttpResponseRedirect('/patient_login')


def specialist_login_check(request):
    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid():

            user = authenticate(request, username=request.POST["username"], password=request.POST["password"])

            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/specialist_login")
        else:
            return HttpResponseRedirect('specialist_login')
    else:
        return HttpResponseRedirect('specialist_login')


def search_specialist(request):
    if request.method == "GET":
        term = request.GET["term"]
        specialist_list = list(Specialist.objects.filter(user__username__contains=term))
        print(specialist_list)
        id_list = []
        for specialist in specialist_list:
            id_list.append(specialist.pk)
        return JsonResponse(id_list, safe=False)

    else:
        return JsonResponse([], safe=False)


def get_specialist_with_spec(request, spec_id):
    speciality = get_object_or_404(Speciality, pk=spec_id)
    specialist_list = list(Specialist.objects.filter(spec=speciality))
    id_list = []
    for specialist in specialist_list:
        id_list.append(specialist.pk)
    return JsonResponse(id_list, safe=False)


def new_comment(request, specialist_id):
    if request.method == "GET":
        specialist = get_object_or_404(Specialist, pk=specialist_id)
        return render(request, 'new_comment.html', {'specialist': specialist})


@csrf_exempt
def new_comment_check(request):
    if request.method == "POST":
        if (not request.user.is_authenticated) or request.user.is_patient == False:
            return HttpResponse("Login required")

        form = CommentForm(request.POST)
        if form.is_valid():
            user = request.user
            patient = get_object_or_404(Patient, pk=user)
            specialist = get_object_or_404(Specialist, pk=request.POST["specialist_id"])
            comment = Comment(patient_id=patient, specialist_id=specialist,
                              text=request.POST["text"], score=request.POST["score"])
            comment.save()
            return HttpResponse("Successful")
        else:
            return HttpResponse("Failed")
    else:
        return HttpResponse("Failed")


@csrf_exempt
def get_specialist_score(request):
    if request.method == "GET":
        specialist = get_object_or_404(Specialist, pk=request.GET["specialist_id"])

        comment_list = list(Comment.objects.filter(specialist_id=specialist))
        sum_score = 0
        counter = len(comment_list)
        for comment in comment_list:
            sum_score += comment.score
        if counter == 0:
            return HttpResponse("No comment")
        return HttpResponse(sum_score / counter)
    else:
        return HttpResponse("Failed")


@csrf_exempt
def get_speciality(request, spec_id):
    speciality = get_object_or_404(Speciality, pk=spec_id)
    return JsonResponse({"name": speciality.name})


@csrf_exempt
def get_specialist(request, specialist_id):
    if request.method == "GET":
        specialist = get_object_or_404(Specialist, pk=specialist_id)
        name = specialist.user.username
        spec = specialist.spec.pk
        number = specialist.number
        online_pay = specialist.online_pay
        experience_years = specialist.experience_years
        address = specialist.address
        phone = specialist.user.phone
        week_days_0 = specialist.week_days_0
        week_days_1 = specialist.week_days_1
        week_days_2 = specialist.week_days_0
        week_days_3 = specialist.week_days_0
        week_days_4 = specialist.week_days_0
        week_days_5 = specialist.week_days_0
        week_days_6 = specialist.week_days_0

        comment_list = list(Comment.objects.filter(specialist_id=specialist))
        sum_score = 0
        number_comment = len(comment_list)
        comment_response = []
        for comment in comment_list:
            sum_score += comment.score
            comment_response.append(
                {"text": comment.text, "owner": comment.patient_id.user.username}
            )
        if number_comment == 0:
            score = 0
        else:
            score = sum_score/number_comment

        stars = round(score)
        return JsonResponse(
            {"name": name, "spec": spec, "number": number, "online_pay": online_pay,
             "experience_years": experience_years, "address": address, "phone": phone,
             "week_days": [week_days_0, week_days_1, week_days_2, week_days_3, week_days_4, week_days_5, week_days_6],
             "score": score, "stars": stars, "number_comment": number_comment, "comment": comment_response
             }
            , safe=False)
    else:
        return HttpResponse("Failed")


def select_role(request):
    return render(request, 'select_role.html')


def patient_profile(request):
    if (not request.user.is_authenticated) or (not request.user.is_patient):
        return HttpResponse("Failed")
    else:
        patient = get_object_or_404(Patient, pk=request.user)
        return render(request, 'patient_profile.html', {"patient": patient})


def specialist_profile(request):
    if (not request.user.is_authenticated) or (not request.user.is_specialist):
        return HttpResponse("Failed")
    else:
        specialist = get_object_or_404(Specialist, pk=request.user)
        return render(request, 'specialist_profile.html', {"specialist": specialist})


def log_out(request):
    logout(request)
    return HttpResponseRedirect("/")


def update_patient(request):
    if request.method == "POST":
        if (not request.user.is_authenticated) or (not request.user.is_patient):
            return HttpResponseRedirect("patient_profile")
        else:
            get_user_model().objects.filter(pk=request.user.pk).update(phone=request.POST["phone"],
                                                                       username=request.POST["username"])

            if request.POST["password"] != "":
                user = get_user_model().objects.get(pk=request.user.pk)
                user.set_password(request.POST["password"])
                user.save()
            return HttpResponseRedirect("patient_profile")
    else:
        return HttpResponseRedirect("patient_profile")
