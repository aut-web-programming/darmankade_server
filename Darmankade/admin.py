from django.contrib import admin
from .models import Patient, Specialist, Comment, Speciality, User

# Register your models here.
admin.site.register(Patient)
admin.site.register(Specialist)
admin.site.register(Comment)
admin.site.register(Speciality)
admin.site.register(User)

